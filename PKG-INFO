Metadata-Version: 2.1
Name: trytond_sale_subscription_asset
Version: 7.0.1
Summary: A module to handle asset in the sale subscriptions
Home-page: http://www.tryton.org/
Download-URL: http://downloads.tryton.org/7.0/
Author: Tryton
Author-email: foundation@tryton.org
License: GPL-3
Project-URL: Bug Tracker, https://bugs.tryton.org/
Project-URL: Documentation, https://docs.tryton.org/
Project-URL: Forum, https://www.tryton.org/forum
Project-URL: Source Code, https://code.tryton.org/tryton
Classifier: Development Status :: 5 - Production/Stable
Classifier: Environment :: Plugins
Classifier: Framework :: Tryton
Classifier: Intended Audience :: Developers
Classifier: Intended Audience :: Financial and Insurance Industry
Classifier: Intended Audience :: Legal Industry
Classifier: License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)
Classifier: Natural Language :: Bulgarian
Classifier: Natural Language :: Catalan
Classifier: Natural Language :: Chinese (Simplified)
Classifier: Natural Language :: Czech
Classifier: Natural Language :: Dutch
Classifier: Natural Language :: English
Classifier: Natural Language :: Finnish
Classifier: Natural Language :: French
Classifier: Natural Language :: German
Classifier: Natural Language :: Hungarian
Classifier: Natural Language :: Indonesian
Classifier: Natural Language :: Italian
Classifier: Natural Language :: Persian
Classifier: Natural Language :: Polish
Classifier: Natural Language :: Portuguese (Brazilian)
Classifier: Natural Language :: Romanian
Classifier: Natural Language :: Russian
Classifier: Natural Language :: Slovenian
Classifier: Natural Language :: Spanish
Classifier: Natural Language :: Turkish
Classifier: Natural Language :: Ukrainian
Classifier: Operating System :: OS Independent
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.8
Classifier: Programming Language :: Python :: 3.9
Classifier: Programming Language :: Python :: 3.10
Classifier: Programming Language :: Python :: 3.11
Classifier: Programming Language :: Python :: 3.12
Classifier: Programming Language :: Python :: Implementation :: CPython
Classifier: Topic :: Office/Business
Requires-Python: >=3.8
License-File: LICENSE
Requires-Dist: trytond_sale_subscription<7.1,>=7.0
Requires-Dist: trytond_stock_lot<7.1,>=7.0
Requires-Dist: trytond<7.1,>=7.0
Provides-Extra: test
Requires-Dist: proteus<7.1,>=7.0; extra == "test"

Sale Subscription Asset Module
##############################

The sale subscription asset module adds the notion of asset to the sale
subscription module.

Two fields are added to the *Service*:

    - *Lots*: All the lots that can be used when providing this service
    - *Available Lots*: A subset of the preceding field displaying the
      available lots

On the *Subscription Line*, it is possible to specify the lot to use. When the
subscription will be running this field will become required for service with
lots.

On the *Lot*, the field *Subscribed* will point to the line of the subscription
currently using this lot.
